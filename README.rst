==========
pyMSDtorch
==========


.. image:: https://img.shields.io/pypi/v/pyMSDtorch.svg
        :target: https://pypi.python.org/pypi/pyMSDtorch

.. image:: https://img.shields.io/travis/phzwart/pyMSDtorch.svg
        :target: https://travis-ci.com/phzwart/pyMSDtorch

.. image:: https://readthedocs.org/projects/pyMSDtorch/badge/?version=latest
        :target: https://pymsdtorch.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Python Boilerplate contains all the boilerplate you need to create a Python package.


* Free software: Check the license for details
* Documentation: https://pymsdtorch.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
