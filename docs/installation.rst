.. highlight:: shell

============
Installation
============


Stable release
--------------

To install pyMSDtorch, run these commands in a empty directory:

Clone the public repository:

.. code-block:: console

    $ git clone https://bitbucket.org/berkeleylab/pymsdtorch.git

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ pip install -e .

Or do it all at once using

