import numpy as np
import matplotlib.pyplot as plt
import skimage.transform
import tqdm
import h5py


class Shape(object):
    """
    Base class for shapes.
    """
    def __init__(self, Nxy=64):
        """
        Base class for building shapes.
        :param Nxy: The size of the canvas we paint on
        """
        self.Nxy = Nxy
        self.mean = np.array([Nxy / 2.0, Nxy / 2.0])
        self.canvas = np.zeros((self.Nxy, self.Nxy))
        x = np.arange(0, Nxy)
        self.X, self.Y = np.meshgrid(x, x)

    def get_random_rotation(self):
        """
        Rotates the canvas in a random fashion
        :return: return image in random rotation
        """
        phi = np.random.uniform(0, 360, 1)[0]
        tmp = skimage.transform.rotate(self.canvas, phi)
        return tmp


class Circle(Shape):
    def __init__(self, radius=0.25, Nxy=64):
        """
        Build a circle with radius
        :param radius: The radius
        :param Nxy: canmvas size
        """
        super().__init__(Nxy=Nxy)
        self.radius = radius*Nxy

        R = (self.X - self.mean[0]) ** 2.0 + (self.Y - self.mean[1]) ** 2.0
        R = np.sqrt(R)
        sel = R < self.radius
        self.canvas[sel] = 1.0


class Rectangle(Shape):
    def __init__(self, height=0.20, width=0.05, Nxy=64):
        """
        Build a rectangle
        :param height: height
        :param width: width
        :param Nxy: canvas size
        """
        super().__init__(Nxy=Nxy)
        self.height = height*Nxy
        self.width = width*Nxy

        dX = np.abs(self.X - self.mean[0])
        dY = np.abs(self.Y - self.mean[1])

        sel = (dX < self.height) & (dY < self.width)
        self.canvas[sel] = 1.0


class Triangle(Rectangle):
    def __init__(self, height=0.25, Nxy=64):
        """
        Build an equilateral right triangle
        :param height: The height
        :param Nxy: canvas size
        """
        super().__init__(height=height, width=height, Nxy=Nxy)
        self.canvas = np.triu(self.canvas)


class Donut(Shape):
    def __init__(self, radius=.5, width=.15, Nxy=64):
        """
        Build a donut
        :param radius: Radius of circle
        :param width: Ring width
        :param Nxy: canvas size
        """
        super().__init__(Nxy=Nxy)
        self.radius = radius
        self.width = width*radius
        outer = Circle(radius, Nxy)
        inner = Circle(radius - width, Nxy)
        self.canvas = outer.canvas - inner.canvas


def random_rectangle(width=(0.05, 0.20), height=(0.20, 0.35), Nxy=64):
    """
    Build a rectangle of random size
    :param width: width range
    :param height: height range
    :param Nxy: canvas size
    :return: a random rectangle
    """
    this_width = np.random.uniform() * (width[1] - width[0]) + width[0]
    this_height = np.random.uniform() * (height[1] - height[0]) + height[0]
    obj = Rectangle(this_width, this_height, Nxy)
    return obj.get_random_rotation()


def random_triangle(width=(0.05, 0.35), Nxy=64):
    """
    Build a random right equilateral triangle
    :param width: width range
    :param Nxy: canvas size
    :return: a random triangle
    """
    this_width = np.random.uniform() * (width[1] - width[0]) + width[0]
    obj = Triangle(this_width, Nxy)
    return obj.get_random_rotation()


def random_circle(radius=(0.05, 0.40), Nxy=64):
    """
    Build a random circle

    :param radius: radius range
    :param Nxy: canvas size
    :return: a random circle
    """
    this_radius = np.random.uniform() * (radius[1] - radius[0]) + radius[0]
    obj = Circle(this_radius, Nxy)
    return obj.get_random_rotation()


def random_donut(radius=[0.05, 0.40], width=[0.20, 0.70], Nxy=64):

    """
    Build a random donut
    :param radius: radius range
    :param width: fractional width range, relatrive to radius
    :param Nxy: canvas size
    :return: a random donut
    """
    this_radius = np.random.uniform() * (radius[1] - radius[0]) + radius[0]
    this_width = np.random.uniform() * (width[1] - width[0]) + width[0]
    this_width = this_radius * this_width
    obj = Donut(this_radius, this_width, Nxy)
    return obj.get_random_rotation()


def get_random_object(noise_level=0.1, Nxy=64):
    """
    Build a ranomd shape with fixed uniform noise level
    :param noise_level: uniform noise level
    :param Nxy: canvas size
    :return: a random shape
    """
    names = ["rectangle", "circle", "triangle", "donut"]
    obj_gen = [random_rectangle, random_circle, random_triangle, random_donut]
    ind = np.random.randint(0, 4)
    img = obj_gen[ind](Nxy=Nxy)
    noise = np.random.uniform(0, noise_level, img.shape)

    class_img = img*1.0
    sel = class_img > 0.5
    class_img[sel]=1
    class_img[~sel]=0
    class_img = class_img*(ind+1)
    norma = img+noise - np.min(img+noise)
    div = np.max(norma)
    if np.max(norma) < 1e-12:
        div = 1.0
    norma = norma / div
    return img, img+noise, class_img, norma, names[ind]

def build_random_shape_set(N_train, N_test, N_validate, noise_level=0.1, Nxy=64):
    """
    Build 3 h5 files containing test data with random shapes.
    Uses standard filenames: train_shapes_2d.hdf5, etc etc

    :param N_train: Number of training images
    :param N_test: Number of test images
    :param N_validate: Number of validation images
    :param noise_level: noise level
    :param Nxy: canvas size
    :return:
    """
    f = h5py.File('train_shapes_2d.hdf5', 'w')
    dset_imgs = f.create_dataset("shape_GT", (N_train, Nxy, Nxy), dtype='f')
    dset_obsr = f.create_dataset("shape_obs", (N_train, Nxy, Nxy), dtype='f')
    dset_clss = f.create_dataset("shape_class", (N_train, Nxy, Nxy), dtype='i')
    dset_norm = f.create_dataset("shape_norma", (N_train, Nxy, Nxy), dtype='f')
    for ii in tqdm.tqdm(range(N_train)):
        gt, obs, clss, norma, _ = get_random_object(noise_level, Nxy)
        dset_imgs[ii, :, :] = gt
        dset_obsr[ii, :, :] = obs
        dset_clss[ii, :, :] = clss
        dset_norm[ii, :, :] = norma
    f.close()




    f = h5py.File('test_shapes_2d.hdf5', 'w')
    dset_imgs = f.create_dataset("shape_GT", (N_test, Nxy, Nxy), dtype='f')
    dset_obsr = f.create_dataset("shape_obs", (N_test, Nxy, Nxy), dtype='f')
    dset_clss = f.create_dataset("shape_class", (N_test, Nxy, Nxy), dtype='i')
    dset_norm = f.create_dataset("shape_norma", (N_test, Nxy, Nxy), dtype='f')
    for ii in tqdm.tqdm(range(N_test)):
        gt, obs, clss, norma, _ = get_random_object(noise_level, Nxy)
        dset_imgs[ii, :, :] = gt
        dset_obsr[ii, :, :] = obs
        dset_clss[ii, :, :] = clss
        dset_norm[ii, :, :] = norma
    f.close()

    f = h5py.File('validate_shapes_2d.hdf5', 'w')
    dset_imgs = f.create_dataset("shape_GT", (N_validate, Nxy, Nxy), dtype='f')
    dset_obsr = f.create_dataset("shape_obs", (N_validate, Nxy, Nxy), dtype='f')
    dset_clss = f.create_dataset("shape_class", (N_validate, Nxy, Nxy), dtype='i')
    dset_norm = f.create_dataset("shape_norma", (N_validate, Nxy, Nxy), dtype='f')
    for ii in tqdm.tqdm(range(N_validate)):
        gt, obs, clss, norma, _ = get_random_object(noise_level, Nxy)
        dset_imgs[ii, :, :] = gt
        dset_obsr[ii, :, :] = obs
        dset_clss[ii, :, :] = clss
        dset_norm[ii, :, :] = norma
    f.close()






