import os
import h5py
import tqdm
import numpy as np
from pyMSDtorch.test_data.twoD import noisy_gauss_2d
from pyMSDtorch.test_data.twoD import noisy_gauss_2d_time


def build_data_standard_sets_2d(N_imgs=1000,
                                N_peaks=3,
                                N_xy=64,
                                SNR=1.0,
                                mask_radius=2.0, chunk=10):
    engine_B = noisy_gauss_2d.DataMaker(N_peaks=N_peaks,
                                        N_xy=N_xy)

    ### Build Training Set ###
    ##########################
    # #N_imgs = 10000
    # N_xy = 64

    f = h5py.File('train_data_2d.hdf5', 'w')

    dset_imgs = f.create_dataset("trax_GT", (N_imgs, N_xy, N_xy), dtype='f')
    dset_msks = f.create_dataset("trax_mask", (N_imgs, N_xy, N_xy), dtype='f')
    dset_clss = f.create_dataset("trax_classes", (N_imgs, 2, N_xy, N_xy), dtype='f')
    dset_obs = f.create_dataset("trax_obs", (N_imgs, N_xy, N_xy), dtype='f')
    dset_obs_norm = f.create_dataset("trax_obs_norm", (N_imgs, N_xy, N_xy), dtype='f')

    # Lets fill up this file in chunks, making sure we don't fill our memory

    for ii in tqdm.tqdm(range(int(N_imgs // chunk))):
        B_imgs, B_msks, B_n_imgs, B_n_imgs_norm, B_c_class = engine_B.generate_data_with_normal_noise(
            M_images=chunk, SNR=SNR, mask_radius=mask_radius)

        dset_imgs[ii * chunk:(ii + 1) * chunk, :, :] = B_imgs
        dset_msks[ii * chunk:(ii + 1) * chunk, :, :] = B_msks
        dset_obs[ii * chunk:(ii + 1) * chunk, :, :] = B_n_imgs
        dset_obs_norm[ii * chunk:(ii + 1) * chunk, :, :] = B_n_imgs_norm
        dset_clss[ii * chunk:(ii + 1) * chunk, :, :, :] = B_c_class

    # close the h5 file
    f.close()

    ### Build Validation Set ###
    ############################
    N_imgs = max(100,np.floor(N_imgs * .1))
    # N_xy = 64

    f = h5py.File('validate_data_2d.hdf5', 'w')

    dset_imgs = f.create_dataset("trax_GT", (N_imgs, N_xy, N_xy), dtype='f')
    dset_msks = f.create_dataset("trax_mask", (N_imgs, N_xy, N_xy), dtype='f')
    dset_clss = f.create_dataset("trax_classes", (N_imgs, 2, N_xy, N_xy), dtype='f')
    dset_obs = f.create_dataset("trax_obs", (N_imgs, N_xy, N_xy), dtype='f')
    dset_obs_norm = f.create_dataset("trax_obs_norm", (N_imgs, N_xy, N_xy), dtype='f')

    # Lets fill up this file in chunks, making sure we don't fill our memory

    for ii in tqdm.tqdm(range(int(N_imgs // chunk))):
        B_imgs, B_msks, B_n_imgs, B_n_imgs_norm, B_c_class = engine_B.generate_data_with_normal_noise(
            M_images=chunk, SNR=SNR, mask_radius=mask_radius)
        dset_imgs[ii * chunk:(ii + 1) * chunk, :, :] = B_imgs
        dset_msks[ii * chunk:(ii + 1) * chunk, :, :] = B_msks
        dset_obs[ii * chunk:(ii + 1) * chunk, :, :] = B_n_imgs
        dset_obs_norm[ii * chunk:(ii + 1) * chunk, :, :] = B_n_imgs_norm
        dset_clss[ii * chunk:(ii + 1) * chunk, :, :, :] = B_c_class

    # close the h5 file
    f.close()

    ### Build Testing Set ###
    ##########################

    # N_xy = 64

    f = h5py.File('test_data_2d.hdf5', 'w')

    dset_imgs = f.create_dataset("trax_GT", (N_imgs, N_xy, N_xy), dtype='f')
    dset_msks = f.create_dataset("trax_mask", (N_imgs, N_xy, N_xy), dtype='f')
    dset_clss = f.create_dataset("trax_classes", (N_imgs, 2, N_xy, N_xy), dtype='f')
    dset_obs = f.create_dataset("trax_obs", (N_imgs, N_xy, N_xy), dtype='f')
    dset_obs_norm = f.create_dataset("trax_obs_norm", (N_imgs, N_xy, N_xy), dtype='f')

    # Lets fill up this file in chunks, making sure we don't fill our memory
    chunk = 100
    for ii in tqdm.tqdm(range(int(N_imgs // chunk))):
        B_imgs, B_msks, B_n_imgs, B_n_imgs_norm, B_c_class = engine_B.generate_data_with_normal_noise(
            M_images=chunk, SNR=SNR, mask_radius=mask_radius)

        dset_imgs[ii * chunk:(ii + 1) * chunk, :, :] = B_imgs
        dset_msks[ii * chunk:(ii + 1) * chunk, :, :] = B_msks
        dset_obs[ii * chunk:(ii + 1) * chunk, :, :] = B_n_imgs
        dset_obs_norm[ii * chunk:(ii + 1) * chunk, :, :] = B_n_imgs_norm
        dset_clss[ii * chunk:(ii + 1) * chunk, :, :, :] = B_c_class

    # close the h5 file
    f.close()


def build_data_mixed_level_sets_2d(N_imgs=1000,
                                   N_peaks=3,
                                   N_xy=32,
                                   mask_radius=1.0, chunk=10):
    engine_B = noisy_gauss_2d.MixedNoiseDataMaker(N_peaks=N_peaks,
                                                  N_xy=N_xy)

    noise_limits = np.array( engine_B.SNR_brackets )
    print(noise_limits)


    ### Build Training Set ###
    ##########################

    f = h5py.File('train_data_2d.hdf5', 'w')

    dset_imgs = f.create_dataset("trax_GT", (N_imgs, N_xy, N_xy), dtype='f')
    dset_msks = f.create_dataset("trax_mask", (N_imgs, N_xy, N_xy), dtype='i')
    dset_obs = f.create_dataset("trax_obs", (N_imgs, N_xy, N_xy), dtype='f')
    dset_obs_norm = f.create_dataset("trax_obs_norm", (N_imgs, N_xy, N_xy), dtype='f')

    dset_levels = f.create_dataset("PSNR_brackets", noise_limits.shape, dtype='f')
    dset_levels[:,:] = noise_limits[:,:]


    # Lets fill up this file in chunks

    for ii in tqdm.tqdm(range(int(N_imgs // chunk))):
        B_imgs, B_msks, B_n_imgs, B_n_imgs_norm = engine_B.generate_data_with_normal_noise(M_images=chunk,
                                                                                           mask_radius=mask_radius)
        # print('min/max of noisy: ', np.min(B_n_imgs), np.max(B_n_imgs))

        dset_imgs[ii * chunk:(ii + 1) * chunk, :, :] = B_imgs
        dset_msks[ii * chunk:(ii + 1) * chunk, :, :] = B_msks
        dset_obs[ii * chunk:(ii + 1) * chunk, :, :] = B_n_imgs
        dset_obs_norm[ii * chunk:(ii + 1) * chunk, :, :] = B_n_imgs_norm

    # close the h5 file
    f.close()

    ### Build Validation Set ###
    ############################
    N_imgs = max(100,np.floor(N_imgs * .2))

    f = h5py.File('validate_data_2d.hdf5', 'w')

    dset_imgs = f.create_dataset("trax_GT", (N_imgs, N_xy, N_xy), dtype='f')
    dset_msks = f.create_dataset("trax_mask", (N_imgs, N_xy, N_xy), dtype='f')
    dset_obs = f.create_dataset("trax_obs", (N_imgs, N_xy, N_xy), dtype='f')
    dset_obs_norm = f.create_dataset("trax_obs_norm", (N_imgs, N_xy, N_xy), dtype='f')

    dset_levels = f.create_dataset("PSNR_brackets", noise_limits.shape, dtype='f')
    dset_levels = noise_limits

    # Lets fill up this file in chunks, making sure we don't fill our memory
    for ii in tqdm.tqdm(range(int(N_imgs // chunk))):
        B_imgs, B_msks, B_n_imgs, B_n_imgs_norm = engine_B.generate_data_with_normal_noise(M_images=chunk,
                                                                                           mask_radius=mask_radius)
        # print('min/max of noisy: ', np.min(B_n_imgs), np.max(B_n_imgs))
        dset_imgs[ii * chunk:(ii + 1) * chunk, :, :] = B_imgs
        dset_msks[ii * chunk:(ii + 1) * chunk, :, :] = B_msks
        dset_obs[ii * chunk:(ii + 1) * chunk, :, :] = B_n_imgs
        dset_obs_norm[ii * chunk:(ii + 1) * chunk, :, :] = B_n_imgs_norm

    # close the h5 file
    f.close()

    ### Build Testing Set ###
    ##########################
    f = h5py.File('test_data_2d.hdf5', 'w')

    dset_imgs = f.create_dataset("trax_GT", (N_imgs, N_xy, N_xy), dtype='f')
    dset_msks = f.create_dataset("trax_mask", (N_imgs, N_xy, N_xy), dtype='f')
    dset_obs = f.create_dataset("trax_obs", (N_imgs, N_xy, N_xy), dtype='f')
    dset_obs_norm = f.create_dataset("trax_obs_norm", (N_imgs, N_xy, N_xy), dtype='f')

    dset_levels = f.create_dataset("PSNR_brackets", noise_limits.shape, dtype='f')
    dset_levels = noise_limits

    # Lets fill up this file in chunks, making sure we don't fill our memory
    chunk = 100
    for ii in tqdm.tqdm(range(int(N_imgs // chunk))):
        B_imgs, B_msks, B_n_imgs, B_n_imgs_norm = engine_B.generate_data_with_normal_noise(M_images=chunk,
                                                                                           mask_radius=mask_radius)
        dset_imgs[ii * chunk:(ii + 1) * chunk, :, :] = B_imgs
        dset_msks[ii * chunk:(ii + 1) * chunk, :, :] = B_msks
        dset_obs[ii * chunk:(ii + 1) * chunk, :, :] = B_n_imgs
        dset_obs_norm[ii * chunk:(ii + 1) * chunk, :, :] = B_n_imgs_norm

    # close the h5 file
    f.close()


def build_data_standard_sets_2d_time(N_imgs=1000,
                                     K_time_points=8,
                                     N_peaks=3,
                                     sigma=0.02,
                                     trend=0.01,
                                     dxy=0.01,
                                     cc=0.6,
                                     N_xy=64,
                                     normalize=True):
    engine_B = noisy_gauss_2d_time.DataMaker(N_peaks,
                                             sigma=sigma,
                                             trend=trend,
                                             dxy=dxy,
                                             cc=cc,
                                             N_xy=N_xy)

    ### Build Training Set ###
    ##########################
    # N_imgs = 10000
    # K_time_points = 8
    # N_xy = 64

    if normalize:
        f = h5py.File('train_data_2d_time_norm.hdf5', 'w')
    else:
        f = h5py.File('train_data_2d_time.hdf5', 'w')

    dset_imgs = f.create_dataset("trax_GT", (N_imgs, K_time_points, N_xy, N_xy), dtype='f')
    dset_msks = f.create_dataset("trax_mask", (N_imgs, K_time_points, N_xy, N_xy), dtype='f')
    dset_obs = f.create_dataset("trax_obs", (N_imgs, K_time_points, N_xy, N_xy), dtype='f')

    # Lets fill up this file in chunks, making sure we don't fill our memory
    chunk = 100
    for ii in tqdm.tqdm(range(N_imgs // chunk)):
        B_imgs, B_msks, B_n_imgs = engine_B.generate_data_with_uniform_noise(M_images=chunk,
                                                                             K_time_steps=K_time_points,
                                                                             noise_level=1,
                                                                             mask_radius=2.0)
        dset_imgs[ii * chunk:(ii + 1) * chunk, :, :, :] = B_imgs
        dset_msks[ii * chunk:(ii + 1) * chunk, :, :, :] = B_msks
        dset_obs[ii * chunk:(ii + 1) * chunk, :, :, :] = B_n_imgs

    # close the h5 file
    f.close()

    ### Build Testing Set ###
    #########################
    N_imgs = int(np.floor(N_imgs * .1))  # take 1/10th size for test set
    # K_time_points = 8      #defined as input parameter
    # N_xy = 64              #defined as input parameter

    if normalize:
        f = h5py.File('test_data_2d_time_norm.hdf5', 'w')
    else:
        f = h5py.File('test_data_2d_time.hdf5', 'w')

    dset_imgs = f.create_dataset("trax_GT", (N_imgs, K_time_points, N_xy, N_xy), dtype='f')
    dset_msks = f.create_dataset("trax_mask", (N_imgs, K_time_points, N_xy, N_xy), dtype='f')
    dset_obs = f.create_dataset("trax_obs", (N_imgs, K_time_points, N_xy, N_xy), dtype='f')

    # Lets fill up this file in chunks, making sure we don't fill our memory
    chunk = 100
    for ii in tqdm.tqdm(range(N_imgs // chunk)):
        B_imgs, B_msks, B_n_imgs = engine_B.generate_data_with_uniform_noise(M_images=chunk,
                                                                             K_time_steps=K_time_points,
                                                                             noise_level=1,
                                                                             mask_radius=2.0)
        dset_imgs[ii * chunk:(ii + 1) * chunk, :, :, :] = B_imgs
        dset_msks[ii * chunk:(ii + 1) * chunk, :, :, :] = B_msks
        dset_obs[ii * chunk:(ii + 1) * chunk, :, :, :] = B_n_imgs

    # close the h5 file
    f.close()


if __name__ == "__main__":
    are_you_sure = None
    while are_you_sure not in ["Y", "N"]:
        are_you_sure = input("Are you sure you want to build Test data Y/N: ")
    print("go", are_you_sure)
    if are_you_sure == "Y":
        if os.path.isfile("train_data_2d.hdf5"):
            os.remove("train_data_2d.hdf5")
        if os.path.isfile("test_data_2d.hdf5"):
            os.remove("test_data_2d.hdf5")
        build_data_standard_sets_2d()

        if os.path.isfile("train_data_2d_time.hdf5"):
            os.remove("train_data_2d_time.hdf5")
        if os.path.isfile("test_data_2d_time.hdf5"):
            os.remove("test_data_2d_time.hdf5")
        build_data_standard_sets_2d_time()

    else:
        print("MAKE UP YOUR MIND")
