import os
import numpy as np
import h5py
import tqdm
from pyMSDtorch.test_data.threeD import noisy_gauss_3d


def build_data_standard_sets_3d(N_imgs=1000,
                                N_peaks=1,
                                N_xyz=32,
                                SNR=0.5,
                                mask_radius=2.0):

    engine_B = noisy_gauss_3d.DataMaker3D(N_peaks=N_peaks,
                                          N_xyz=N_xyz)

    # build a training_set
    #N_imgs = 10000

    f = h5py.File('train_data_3d.hdf5', 'w')
    dset_imgs = f.create_dataset("trax_GT", (N_imgs, N_xyz, N_xyz, N_xyz), dtype='f')
    dset_msks = f.create_dataset("trax_mask", (N_imgs, N_xyz, N_xyz, N_xyz), dtype='f')
    dset_obs = f.create_dataset("trax_obs", (N_imgs, N_xyz, N_xyz, N_xyz), dtype='f')

    # Lets fill up this file in chunks, making sure we don't fill our memory
    chunk = 100
    for ii in tqdm.tqdm(range(int(N_imgs // chunk))):
        B_imgs, B_msks, B_n_imgs = engine_B.generate_data_with_gaussian_noise(M_images=chunk,
                                                                             SNR=SNR,
                                                                             mask_radius=mask_radius)
        dset_imgs[ii * chunk:(ii + 1) * chunk, :, :, :] = B_imgs
        dset_msks[ii * chunk:(ii + 1) * chunk, :, :, :] = B_msks
        dset_obs[ii * chunk:(ii + 1) * chunk, :, :, :] = B_n_imgs

    # close the h5 file
    f.close()

    # build a test set
    N_imgs = np.floor(N_imgs * .1)
    if N_imgs < 100:
        N_imgs = 100;
    #N_imgs = 1000

    f = h5py.File('test_data_3d.hdf5', 'w')
    dset_imgs = f.create_dataset("trax_GT", (N_imgs, N_xyz, N_xyz, N_xyz), dtype='f')
    dset_msks = f.create_dataset("trax_mask", (N_imgs, N_xyz, N_xyz, N_xyz), dtype='f')
    dset_obs = f.create_dataset("trax_obs", (N_imgs, N_xyz, N_xyz, N_xyz), dtype='f')

    # Lets fill up this file in chunks, making sure we don't fill our memory
    chunk = 100

    print(N_imgs // chunk)

    for ii in tqdm.tqdm(range(int(N_imgs // chunk))):
        B_imgs, B_msks, B_n_imgs = engine_B.generate_data_with_gaussian_noise(M_images=chunk,
                                                                             SNR=SNR,
                                                                             mask_radius=mask_radius)
        dset_imgs[ii * chunk:(ii + 1) * chunk, :, :, :] = B_imgs
        dset_msks[ii * chunk:(ii + 1) * chunk, :, :, :] = B_msks
        dset_obs[ii * chunk:(ii + 1) * chunk, :, :, :] = B_n_imgs

    # close the h5 file
    f.close()


def build_data_standard_sets_3d_sliced(N_peaks=1,
                                       N_xyz=32,
                                       SNR=0.5):

    engine_B = noisy_gauss_3d.DataMaker3D(N_peaks=N_peaks,
                                          N_xyz=N_xyz)

    # build a test set
    N_imgs = 1000

    f = h5py.File('test_data_3d_sliced.hdf5', 'w')
    dset_imgs = f.create_dataset("trax_GT", (N_imgs*N_xyz, N_xyz, N_xyz), dtype='f')
    dset_msks = f.create_dataset("trax_mask", (N_imgs*N_xyz, N_xyz, N_xyz), dtype='f')
    dset_obs = f.create_dataset("trax_obs", (N_imgs*N_xyz, N_xyz, N_xyz), dtype='f')

    # Lets fill up this file in chunks, making sure we don't fill our memory
    chunk = 100
    for ii in tqdm.tqdm(range(N_imgs // chunk)):
        B_imgs, B_msks, B_n_imgs = engine_B.generate_data_with_gaussian_noise(M_images=chunk,
                                                                             SNR=SNR,
                                                                             mask_radius=2.0)
        newshape = B_imgs.shape
        dset_imgs[ii * chunk * N_xyz:(ii + 1) * chunk * N_xyz, :, :] = B_imgs.reshape(
            newshape[0]*newshape[1], newshape[2], newshape[3]
        )

        dset_msks[ii * chunk * N_xyz:(ii + 1) * chunk * N_xyz, :, :] = B_msks.reshape(
            newshape[0]*newshape[1], newshape[2], newshape[3]
        )

        dset_obs[ii * chunk * N_xyz:(ii + 1) * chunk * N_xyz, :, :] = B_n_imgs.reshape(
            newshape[0] * newshape[1], newshape[2], newshape[3]
        )

    # close the h5 file
    f.close()

    # build a training_set
    N_imgs = 10000

    f = h5py.File('train_data_3d_sliced.hdf5', 'w')
    dset_imgs = f.create_dataset("trax_GT", (N_imgs * N_xyz, N_xyz, N_xyz), dtype='f')
    dset_msks = f.create_dataset("trax_mask", (N_imgs * N_xyz, N_xyz, N_xyz), dtype='f')
    dset_obs = f.create_dataset("trax_obs", (N_imgs * N_xyz, N_xyz, N_xyz), dtype='f')

    # Lets fill up this file in chunks, making sure we don't fill our memory
    chunk = 100
    for ii in tqdm.tqdm(range(N_imgs // chunk)):
        B_imgs, B_msks, B_n_imgs = engine_B.generate_data_with_gaussian_noise(M_images=chunk,
                                                                             SNR=SNR,
                                                                             mask_radius=2.0)
        newshape = B_imgs.shape
        newshape = (newshape[0] * newshape[1], newshape[2], newshape[3])
        dset_imgs[ii * chunk * N_xyz:(ii + 1) * chunk * N_xyz, :, :] = B_imgs.reshape(
            newshape
        )

        dset_msks[ii * chunk * N_xyz:(ii + 1) * chunk * N_xyz, :, :] = B_msks.reshape(
            newshape
        )

        dset_obs[ii * chunk * N_xyz:(ii + 1) * chunk * N_xyz, :, :] = B_n_imgs.reshape(
            newshape
        )
    # close the h5 file
    f.close()



if __name__ == "__main__":
    build_data_standard_sets_3d(N_peaks=1,
                                N_xyz=32,
                                SNR=0.5)
    build_data_standard_sets_3d_sliced(N_peaks=1,
                                       N_xyz=32,
                                       SNR=1.0)