import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots
import numpy as np

def plot_RGB_and_labels(img, labels, plot_params):
    """
    Make a side-by-side plot of an RGB image and a set of labels

    :param img: The rgb image
    :param labels: the heatmap with labels
    :param plot_params: some mplot parameters in a dict
    :return: a graph object
    """


    fig = make_subplots(rows=1, cols=2)

    fig.add_trace(
        go.Heatmap( px.imshow(labels).data[0] ),
        row=1, col=2
    )

    fig.add_trace(
        go.Image( px.imshow(img).data[0] ),
        row=1, col=1
    )

    fig.update_yaxes(autorange="reversed", row=1, col=2)
    fig.update_layout( yaxis=dict(scaleanchor='x', constrain=None) )
    fig.update_layout(title_text=plot_params["title"])
    #TODO: this is ugly, and basically I don;t fully understand how to make a general solution
    # so this is it for now-ish
    fig["layout"]["yaxis2"]["domain"] = [0.05, 0.95]
    return fig


def plot_heatmap_and_labels(img, labels, plot_params):
    """
    Make a side-by-side plot of an RGB image and a set of labels

    :param img: The rgb image
    :param labels: the heatmap with labels
    :param plot_params: some mplot parameters in a dict
    :return: a graph object
    """


    fig = make_subplots(rows=1, cols=2)

    fig.add_trace(
        go.Heatmap( px.imshow(labels).data[0] ),
        row=1, col=2
    )

    fig.add_trace(
        go.Heatmap( px.imshow(img).data[0] ),
        row=1, col=1
    )

    #fig.update_yaxes(autorange="reversed", row=1, col=2)
    fig.update_layout(yaxis=dict(scaleanchor='x', constrain=None) )
    fig.update_layout(title_text=plot_params["title"])

    return fig


def plot_training_results_segmentation( results_dict):
    training_loss = results_dict['Training loss']
    validation_loss = results_dict['Validation loss']
    training_F1 = results_dict['F1 training']
    validation_F1 = results_dict['F1 validation']
    epoch = np.array(range(len(training_loss)))
    fig = make_subplots(rows=1, cols=2, subplot_titles=["Loss", "F1"])
    fig.add_trace(go.Scatter(x=epoch,
                             y=training_loss,
                             mode='lines+markers',
                             name="Training",
                             line=dict(color='red')), col=1, row=1)
    fig.add_trace(go.Scatter(x=epoch,
                             y=validation_loss,
                             mode='lines+markers',
                             name="Validation",
                             line=dict(color='blue')), col=1, row=1)

    fig.update_yaxes(type="log", col=1,row=1)
    fig.add_trace(go.Scatter(x=epoch,
                             y=training_F1,
                             mode='lines+markers', showlegend=False,
                             line=dict(color='red')), col=2, row=1)
    fig.add_trace(go.Scatter(x=epoch,
                             y=validation_F1,
                             mode='lines+markers', showlegend=False,
                             line=dict(color='blue')), col=2, row=1)

    fig.update_yaxes(type="log", col=1,row=1)
    return fig





