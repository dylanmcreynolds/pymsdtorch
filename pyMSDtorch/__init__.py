"""Top-level package for pyMSDtorch."""

__author__ = """Petrus Hendrik Zwart"""
__email__ = 'PHZwart@lbl.gov'
__version__ = '0.0.8'
