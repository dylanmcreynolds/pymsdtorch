"""
Diffgrad code from
https://github.com/shivram1987/diffGrad/blob/master/diffGrad_v2.py
Minor changes introduced to fix deprecation issues.

"""
# Fixes a bug in original diffGrad code
import math
import torch
from torch.optim.optimizer import Optimizer
import numpy as np
import torch.nn as nn


# import torch.optim as Optimizer

class diffgrad(Optimizer):
    """
    Implements diffGrad algorithm. It is modified from the pytorch
    implementation of Adam. It has been proposed in `diffGrad: An Optimization
    Method for Convolutional Neural Networks


    :param params: iterable of parameters to optimize or dicts defining
                   parameter groups
    :param lr: optional learning rate (default: 1e-3)
    :type lr: float
    :param betas: optional coefficients used for computing running averages
                  of gradient and its square (default: (0.9, 0.999))
    :type betas: (Tuple[float, float)
    :param eps: optional term added to the denominator to improve numerical
                stability (default: 1e-8)
    :type eps: float
    :param weight_decay: optional weight decay (L2 penalty) (default: 0)
    :type weight_decay: float
    :param amsgrad: whether to use the AMSGrad variant of this algorithm
                    from the paper `On the Convergence of Adam and Beyond`_
                    (default: False)
    :type amsgrad: boolean

    .. _diffGrad: An Optimization Method for Convolutional Neural Networks:
        https://arxiv.org/abs/1909.11015
    .. _Adam\: A Method for Stochastic Optimization:
        https://arxiv.org/abs/1412.6980
    .. _On the Convergence of Adam and Beyond:
        https://openreview.net/forum?id=ryQu7f-RZ
    """

    def __init__(self, params, lr=1e-3, betas=(0.9, 0.999), eps=1e-8,
                 weight_decay=0):
        if not 0.0 <= lr:
            raise ValueError("Invalid learning rate: {}".format(lr))
        if not 0.0 <= eps:
            raise ValueError("Invalid epsilon value: {}".format(eps))
        if not 0.0 <= betas[0] < 1.0:
            raise ValueError("Invalid beta parameter at index 0: {}".format(betas[0]))
        if not 0.0 <= betas[1] < 1.0:
            raise ValueError("Invalid beta parameter at index 1: {}".format(betas[1]))
        defaults = dict(lr=lr, betas=betas, eps=eps, weight_decay=weight_decay)
        super(diffgrad, self).__init__(params, defaults)

    def __setstate__(self, state):
        super(diffgrad, self).__setstate__(state)

    def step(self, closure=None):
        """Performs a single optimization step.
        Arguments:
            closure (callable, optional): A closure that reevaluates the model
                and returns the loss.
        """
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                grad = p.grad.data
                if grad.is_sparse:
                    raise RuntimeError('diffGrad does not support sparse gradients, please consider SparseAdam instead')

                state = self.state[p]

                # State initialization
                if len(state) == 0:
                    state['step'] = 0
                    # Exponential moving average of gradient values
                    state['exp_avg'] = torch.zeros_like(p.data)
                    # Exponential moving average of squared gradient values
                    state['exp_avg_sq'] = torch.zeros_like(p.data)
                    # Previous gradient
                    state['previous_grad'] = torch.zeros_like(p.data)

                exp_avg, exp_avg_sq, previous_grad = state['exp_avg'], state['exp_avg_sq'], state['previous_grad']
                beta1, beta2 = group['betas']

                state['step'] += 1

                if group['weight_decay'] != 0:
                    grad.add_(group['weight_decay'], p.data)

                # Decay the first and second moment running average coefficient
                exp_avg.mul_(beta1).add_(grad, alpha=1 - beta1)
                exp_avg_sq.mul_(beta2).addcmul_(grad, grad, value=1 - beta2)
                denom = exp_avg_sq.sqrt().add_(group['eps'])

                bias_correction1 = 1 - beta1 ** state['step']
                bias_correction2 = 1 - beta2 ** state['step']

                # compute diffgrad coefficient (dfc)
                diff = abs(previous_grad - grad)
                dfc = 1. / (1. + torch.exp(-diff))
                # state['previous_grad'] = grad %used in paper but has the bug
                # that previous grad is overwritten with grad and diff becomes
                # always zero. Fixed in the next line.
                state['previous_grad'] = grad.clone()

                # update momentum with dfc
                exp_avg1 = exp_avg * dfc

                step_size = group['lr'] * math.sqrt(bias_correction2) / bias_correction1

                p.data.addcdiv_(exp_avg1, denom, value=-step_size)

        return loss

    def tst():
        """
        TODO: Insert some test functionality here
        """

        print('dummy')

    if __name__ == "__main__":
        tst()