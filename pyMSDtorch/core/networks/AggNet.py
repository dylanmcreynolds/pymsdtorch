import torch
import torch.nn as nn
import torch.nn.functional as F

class AggregateNet(nn.Module):
    """
    Final 1x1 convolutional layer that can be used to combine the results
    of multiple individual models

    :param int in_channels: number of channels in the input
    :param int out_channels: number of channels in the output
    :param bool relu: perform ReLU after the layer
    :param bool bias: include a bias parameter in the convolution layer

    """
    def __init__(self, in_channels, out_channels, relu=True, bias=True):
        super().__init__()
        
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.relu = relu
        self.bias = bias

        self.conv = nn.Conv2d(in_channels, out_channels, 
                              kernel_size=1, bias=bias)

    def forward(self, x):
        if self.relu:
            return F.relu(self.conv(x))
        else:
            return self.conv(x)