import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
from pyMSDtorch.core.networks import MSDNet
from pyMSDtorch.core.networks import graph_utils
import torch
import torch.nn as nn
from torch.nn.utils import prune
from pyMSDtorch.core import helpers


def random_DAG_scale_free(n,p):
    G_random = nx.barabasi_albert_graph(n, max(1,int(n*p)))
    g_random = nx.linalg.graphmatrix.adjacency_matrix(G_random).todense()
    g_random = np.triu(g_random)

    rows = np.sum(g_random, axis=0)
    columns = np.sum(g_random, axis=1)

    tots = rows + columns
    names = np.arange(0, 10)
    start_points = np.where((rows == 0) * (tots > 0))[0]
    end_points = np.where((columns == 0) * (tots > 0))[0]
    return g_random, start_points, end_points

def random_DAG(n, p):
    G_random = nx.generators.random_graphs.fast_gnp_random_graph(n, p)
    g_random = nx.linalg.graphmatrix.adjacency_matrix(G_random).todense()
    g_random = np.triu(g_random)

    rows = np.sum(g_random, axis=0)
    columns = np.sum(g_random, axis=1)

    tots = rows + columns
    names = np.arange(0, 10)
    start_points = np.where((rows == 0) * (tots > 0))[0]
    end_points = np.where((columns == 0) * (tots > 0))[0]
    return g_random, start_points, end_points



def sparsity(g):
    N = g.shape[0]
    tot = (N * N - N) / 2
    obs = np.sum(g)
    return obs / tot


def draw_random_dag(n, p, tol=0.01, max_count=100):
    eps = 2.0 / ((n * n - n) / 2)
    if p < eps:
        p = eps
    if tol < eps / 2.0:
        tol = eps / 2.0

    ok = False
    count = 0
    while not ok:
        g, starts, stops = random_DAG(n, p)
        obs_sparsity = sparsity(g)
        if obs_sparsity > 0:
            if np.abs(obs_sparsity - p) < tol:
                ok = True
        count += 1
        if count > max_count:
            print("Having trouble getting desired sparsity, will continue")
            ok = True

    return g


class RandomMultiSourceMultiSinkGraph(object):
    """
    Builds a random graph with specified sparsity parameters.
    Can specify the source and sink parameters, and control
    the distribution of length of skip connections between hidden
    layers.
    """
    def __init__(self,
                 in_channels,
                 out_channels,
                 layers,
                 dilations,
                 LL_alpha=0.25,
                 LL_gamma=1.5,
                 LL_max_degree=None,
                 IL_target=0.15,
                 LO_target=0.15,
                 IO_target=False):
        """

        :param in_channels: Input channeles
        :param out_channels: Output channels
        :param layers: hidden layers
        :param dilations: Dilations choices
        :param LL_alpha: Controls distribution of radius of hidden layer
                         submatrix
        :param LL_gamma: Controls average degree of hidden layer submatrix
        :param LL_max_degree: set the maximum degree of hidden layer nodes
        :param IL_target: Sparsity of input to hidden layer submatrix
        :param LO_target: Sparsity of hidden layer to output submatrix
        :param IO_target: Boolean choice of input to output feedthrough
        :param alpha: controls distribution length of hidden layer connections.
                      When 0, uniform, when large (i.e. 2) only next-neighbour
                      connections are generated.
        """
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.layers = layers
        self.dilation_choices = dilations

        self.LL_alpha = LL_alpha
        self.LL_gamma = LL_gamma
        self.LL_max_degree = LL_max_degree
        self.IL_target = IL_target
        self.IO_target = IO_target
        self.LO_target = LO_target

        #self.these_dilation = self.draw_dilations()

    def draw_dilations(self):
        """
        Draws dilations
        :return: Return a random dilation vector
        """

        tmp = np.random.choice(self.dilation_choices,
                               self.layers,
                               replace=True
                               )
        source_dilations = self.in_channels*[0]
        sink_dilations = self.out_channels*[1]
        tmp = np.hstack( [ source_dilations, tmp, sink_dilations] )
        return tmp

    def build_matrix(self, return_numpy=True):
        """
        Build full random matrix given specifications
        :param show: Shows generated matrix
        :return: the graph
        """
        LL = np.zeros((self.layers, self.layers))  #
        IL = np.zeros((self.in_channels, self.layers))  #
        IO = np.zeros((self.in_channels, self.out_channels))
        LO = np.zeros((self.layers, self.out_channels))

        #########################
        # LL MATRIX CONSTRUCTION
        #########################
        ok = False
        count = 0
        if self.LL_gamma is None:
            LL = np.triu(np.ones((self.layers, self.layers)))
            for ii in range(self.layers):
                LL[ii,ii]=0
            ok=True
        else:
            LL,_,_ = graph_utils.random_LL_graph(self.layers,
                                                 self.LL_alpha,
                                                 1.0,
                                                 self.LL_max_degree
                                                 )

        ##################
        # GRAPH ANALYSIS #
        ##################

        # we now need to identify the 'must have' starting and ending points
        column_sums = np.sum(LL, axis=0)
        row_sums = np.sum(LL, axis=1)
        cols = column_sums == 0
        rows = row_sums != 0
        must_have_inputs = np.where(cols * rows)[0]
        must_have_outputs = np.where(~cols * ~rows)[0]
        in_between_layers = np.where(~cols * rows)[0]
        IL_skip = np.hstack([in_between_layers, must_have_outputs])
        LO_skip = np.hstack([must_have_inputs, in_between_layers])


        #########################
        # IL MATRIX CONSTRUCTION
        #########################

        # CHOICE: ALL INPUT CHANNELS CONNECT TO ALL MANDATORY INPUTS
        IL[:, must_have_inputs] = 1

        # Now we pick at random other connections from I to L
        # Again, I don't want to favor one input channel over another
        # So I just connect all of them
        N_picks = int(len(IL_skip) * self.IL_target + 0.5)
        choices = IL_skip
        these_layers = np.random.choice(choices, N_picks, False)
        IL[:, these_layers] = 1

        #########################
        # LO MATRIX CONSTRUCTION
        #########################

        # Now we need to identify the layer to output layer
        # All Mandatory outputs are linked to all output channels
        LO[must_have_outputs, :] = 1

        # Randomize skips as before
        N_picks = int(len(LO_skip) * self.LO_target + 0.5)
        choices = LO_skip
        these_layers = np.random.choice(choices, N_picks, False)
        LO[these_layers, :] = 1

        #########################
        # IO MATRIX CONSTRUCTION
        #########################

        # As before, I don't want to favor one channel over another
        # this is equivalent to either connecting input channels
        # to output or not.
        if self.IO_target:
            IO = IO + 1

        full_matrix = np.zeros((self.in_channels + self.layers + self.out_channels,
                                self.in_channels + self.layers + self.out_channels))

        full_matrix[0:self.in_channels,
        self.in_channels:self.in_channels + self.layers] = IL

        full_matrix[0:self.in_channels, self.in_channels + self.layers:] = IO

        full_matrix[self.in_channels:self.in_channels + self.layers,
        self.in_channels:self.in_channels + self.layers] = LL

        full_matrix[self.in_channels:self.in_channels + self.layers,
        self.in_channels + self.layers:] = LO


        # Something is amiss here... return_numpy = False returns G, which is
        # densely connected
        if not return_numpy:
            G = nx.from_numpy_matrix(full_matrix, create_using=nx.DiGraph())
            return G

        return full_matrix






class RMSNet(MSDNet.MixedScaleDenseNetwork, RandomMultiSourceMultiSinkGraph):
    """
    Zeros out specific network convolutional channels based on
    incidence graph inherited from parent class RandomMultiSourceMulti-
    SinkGraph
    """

    def __init__(self,
                 in_channels,
                 out_channels,
                 network=None,
                 graph=None,
                 num_layers=10,
                 layer_width=1,
                 max_dilation=10,
                 custom_MSDNet=None,
                 activation=None,
                 normalization=None,
                 final_layer=None,
                 kernel_size=3,
                 dropout=None,
                 convolution=nn.Conv2d,
                 padding_mode="zeros",
                 LL_target=0.15,
                 IL_target=0.15,
                 LO_target=0.15,
                 IO_target=False,
                 alpha=None
                 ):
        MSDNet.MixedScaleDenseNetwork.__init__(self,
                                               in_channels,
                                               out_channels,
                                               num_layers=10,
                                               layer_width=1,
                                               max_dilation=10,
                                               custom_MSDNet=None,
                                               activation=None,
                                               normalization=None,
                                               final_layer=None,
                                               kernel_size=3,
                                               dropout=None,
                                               convolution=nn.Conv2d,
                                               padding_mode="zeros"
                                               )
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.network = network
        self.graph = graph
        self.num_layers = num_layers
        self.layer_width = layer_width
        self.max_dilation = max_dilation
        self.custom_MSDNet = custom_MSDNet
        self.activation = activation
        self.normalization = normalization
        self.final_layer = final_layer
        self.kernel_size = kernel_size
        self.dropout = dropout
        self.convolution = convolution
        self.padding_mode = padding_mode

        RandomMultiSourceMultiSinkGraph.__init__(self,
                                                 in_channels,
                                                 out_channels,
                                                 layers=10,
                                                 dilations=None,
                                                 LL_target=0.15,
                                                 IL_target=0.15,
                                                 LO_target=0.15,
                                                 IO_target=False,
                                                 alpha=0.01
                                                 )
        self.layers = num_layers
        # self.dilations = None
        self.LL_target = LL_target
        self.IL_target = IL_target
        self.LO_target = LO_target
        self.IO_target = IO_target
        self.alpha = alpha

        # super(MSDNet.MixedScaleDenseNetwork, self).__init__()
        # super(RandomMultiSourceMultiSinkGraph, self).__init__()

        # self.in_channels = in_channels
        # self.out_channels = out_channels
        # self.layers = num_layers

        # RandomMultiSourceMultiSinkGraph.build_matrix(self)

    def createRMSNet(self):

        if self.network is None:
            network = MSDNet.MixedScaleDenseNetwork(
                in_channels=self.in_channels,
                out_channels=self.out_channels,
                num_layers=self.num_layers,
                layer_width=self.layer_width,
                max_dilation=self.max_dilation,
                custom_MSDNet=self.custom_MSDNet,
                activation=self.activation,
                normalization=self.normalization,
                final_layer=self.final_layer,
                kernel_size=self.kernel_size,
                dropout=self.dropout,
                convolution=self.convolution,
                padding_mode=self.padding_mode
                )

        if self.graph is None:
            graph = RandomMultiSourceMultiSinkGraph.build_matrix(self, True)
            graph = np.triu(graph,1)


        if self.network is None and self.graph is None:
            network = self.zero_out_layers(network,
                                           graph,
                                           mode='weight')
            return network, graph
        else:
            self.network = self.zero_out_layers(self.network,
                                                self.graph,
                                                mode='gradient')
            return self.network, self.graph



    def zero_out_layers(self, network, adjacencyMatrix, mode='weight'):
        """
        TODO: Comment, clean, and get rid of input modes since we now only need to call once
        """
        isT = torch.is_tensor(adjacencyMatrix)
        if isT is False:
            adjacencyMatrix = torch.from_numpy(adjacencyMatrix)

        aa = (adjacencyMatrix == 0) + (adjacencyMatrix == 1)
        if False in aa:
            print('non binary entry found')

        device = helpers.get_device()
        network.to(device)

        current_layer = 1
        #current_channels = helpers.get_in_channels(network)
        #num_layers = helpers.count_conv2d(network)

        current_channels = self.in_channels
        num_layers = self.num_layers

        with torch.no_grad():
            for m in network.modules():
                if isinstance(m, nn.Conv2d):
                    if current_layer != (num_layers+1):
                        tmp = adjacencyMatrix[0:current_channels, current_channels]
                        if 0 in tmp:
                            # print(m)
                            tmp = (tmp == 0)
                            kernel_size = m.kernel_size[0]
                            # print(m.weight[0][tmp])
                            #print('In loop')
                            if mode == 'weight':
                                #print(m)
                                m.weight[0][tmp] = torch.zeros(
                                    [kernel_size, kernel_size]).to(device)
                                TargetedPruning_unstructured(m, name='weight')
                            elif mode == 'gradient':
                                m.weight.grad[0][tmp] = torch.zeros(
                                    [kernel_size, kernel_size]).to(device)
                            else:
                                raise ValueError(
                                    'Indicate mode with either weight or gradient')
                            # print(m.weight)
                    elif current_layer == num_layers:
                        for j in range(0, m.out_channels):
                            tmp = adjacencyMatrix[0:current_channels,
                                  current_channels + j]
                            if 0 in tmp:
                                tmp = (tmp == 0)
                                kernel_size = m.kernel_size[0]
                                if mode == 'weight':
                                    m.weight[j][tmp] = 0
                                    #TargetedPruning_unstructured(m, name='weight')
                                elif mode == 'gradient':
                                    m.weight.grad[j][tmp] = 0
                                else:
                                    raise ValueError(
                                        'Indicate mode with either weight or gradient')
                    current_channels = current_channels + 1
                    current_layer = current_layer + 1

        return network

class TargetedPruning(prune.BasePruningMethod):
    PRUNING_TYPE = "unstructured"

    #def __init__(self, target):
    #    self.target = target

    def compute_mask(self, tensor, default_mask):
        return torch.abs(tensor) != 0

def TargetedPruning_unstructured(module, name):
    TargetedPruning.apply(module, name)
    return module


def tst():
    in_channels = 1
    out_channels = 1
    num_layers = 5  # Benchmarks used 100, 150, and 200

    obj = RMSNet(in_channels,
                 out_channels,
                 num_layers=num_layers,
                 LL_target=.2,
                 LO_target=.2,
                 IO_target=.2,
                 IL_target=.1,
                 alpha=None)
    net, g = obj.createRMSNet()

    print(g)

    module = net.layer_3.conv_0
    print('')
    print('weights of index 3:')
    # print(list(module.named_parameters()))
    print(module.weight)

    print('buffers:')
    print(list(module.named_buffers()))

    module = net.final_convolution
    print('')
    print('weights of index 5:')
    # print(list(module.named_parameters()))
    print(module.weight)

    print('buffers:')
    print(list(module.named_buffers()))


    assert 1==0

    cumulative = 0.0
    links = []
    MM = 1000
    for ii in range(MM):
        obj = RandomMultiSourceMultiSinkGraph(1, 1, 20, np.arange(5) + 1,
                                      LL_target=0.05,
                                      IL_target=0.0,
                                      LO_target=0.0,
                                      IO_target=False,
                                      alpha=None)


        g = obj.build_matrix(False)
        link = np.sum(g, axis=0)
        link = len( np.where(link>0)[0] )
        links.append(link)
        cumulative += g

    cumulative = cumulative / MM

    plt.imshow(cumulative)
    plt.colorbar()
    plt.show()

    plt.hist(links, bins=100)
    plt.show()


if __name__ == "__main__":
    tst()
