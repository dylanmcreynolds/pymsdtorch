import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

class random_graph_MT(object):
    def __init__(self, N, alpha=1.0, min_degree=None,
                 max_degree=None, gamma=2.5):
        self.alpha = alpha
        self.N = N
        self.min_degree = min_degree
        self.max_degree = max_degree
        self.gamma = gamma

    def node_model(self, ii):
        # build a node connectivity propability
        dnode = np.arange(1, self.N - ii)
        ps = np.exp(-self.alpha * dnode)
        ps = ps / np.sum(ps)
        con_node = dnode + ii

        # determine the degree of this node
        k_max = self.N - ii
        k_min = 1

        # enforce the maximal degree
        if self.max_degree is not None:
            k_max = min(k_max, self.max_degree)
        # enforce the minimum degree if we have to
        if self.min_degree is not None:
            k_min = max(k_min, self.min_degree)
            k_min = min(k_min, self.N - ii - 1)
        degrees = np.arange(k_min, k_max)
        pd = degrees ** (-self.gamma)
        pd = pd / np.sum(pd)

        # now do the sampling of nodes, without replacement
        # first determin the actual degree
        this_degree = np.random.choice(a=degrees, size=1, p=pd)[0]

        # now we need to draw this_degree nodes
        connected_nodes = np.random.choice(a=con_node, size=this_degree,
                                           p=ps, replace=False)
        return connected_nodes

    def random_graph(self):
        G = nx.DiGraph()
        for ii in range(self.N - 1):
            cons = self.node_model(ii)
            for c in cons:
                G.add_edge(ii + 1, c + 1)
        return G

def random_LL_graph(N, alpha, gamma, max_degree=None, return_graph=False):
    obj =random_graph_MT(N, alpha=alpha, gamma=gamma, max_degree=max_degree)
    G = obj.random_graph()
    if return_graph:
        return G

    g_random = nx.linalg.graphmatrix.adjacency_matrix(G).todense()
    g_random = np.triu(g_random)
    plt.imshow(g_random)
    plt.show()

    rows = np.sum(g_random, axis=0)
    columns = np.sum(g_random, axis=1)

    tots = rows + columns
    names = np.arange(0, 10)
    start_points = np.where((rows == 0) * (tots > 0))[0]
    end_points = np.where((columns == 0) * (tots > 0))[0]
    return g_random, start_points, end_points

def assign_size(G, powers, p_power=None):
    if p_power is None:
        p_power = np.ones( len(powers) )
        p_power = p_power / np.sum(p_power)

    for ii, node in enumerate(G.nodes()):
        this_power = np.random.choice( powers, 1, p=p_power)
        G.nodes[node]["size"]= this_power[0]
    return G

def tst():
    G = random_LL_graph(alpha=10.25, gamma=0.0, N=5, return_graph=True)
    powers = np.array( [0,-1,-2,-3] )
    p_power = np.array( [2.0,1.0,1.0,1.0] )
    p_power = p_power / np.sum(p_power)
    assign_size(G,powers,p_power)






if __name__ == "__main__":
    tst()











