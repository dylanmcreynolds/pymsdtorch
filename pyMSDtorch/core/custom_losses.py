import numpy
import torch
import torch.nn as nn
import torch.nn.functional as F

"""
Modules containing popular loss functions suitable for image 
segmentation. These loss functions provide varying metrics for judging 
overall network error and performance, a few of which are simple 
averages of others. Additionally, these loss functions leverage torch.nn  
backend functions and are compatible  with GPU implementation and 
Pytorch autograd gradient calculation.

Though this set of functions is intended for binary image segmentation 
of single classes, it acts as a template for averaging multiple classes.

Overviews of these loss functions are detailed in:
    1) https://arxiv.org/pdf/2005.13449.pdf
    2) https://arxiv.org/pdf/2006.14822.pdf
    
Many loss functions modified from :
    1) https://www.kaggle.com/bigironsphere/loss-function-library-keras-pytorch
"""

class DiceLoss(nn.Module):
    """
    Creates a criterion that outputs the popular Dice score coefficient
    (DSC), a measure ofboverlap between image regions. It is widely used
    in computer vision for edge detection and is generally compared
    against the ubiquitous Binary Cross-Entropy in segmentation tasks.

    Overall, the Dice loss is a general measure for assessing
    segmentation performance when a ground truth is available, though it
    lacks proper re-weighting when dealing with an imbalance of classes
    (i.e. a single class or two are disproportionately represented).
    """

    # Constructor defines hyperparameters
    def __init__(self, smooth=1):
        """:param float smooth: used to avoid division by 0"""

        super(DiceLoss, self).__init__()
        self.smooth = smooth

    def forward(self, inputs, targets):
        """
        :param inputs: tensor of size (N,∗), where N is the batch size
                       and * indicates any number of additional
                       dimensions
        :type inputs: List[float]
        :param targets: tensor of size (N,*), the same size as the input
        :type targets: List[float]
        """

        # Comment out if your model contains a sigmoid or equivalent activation
        # inputs = F.sigmoid(inputs)

        # Flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        intersection = (inputs * targets).sum()
        dice = (2. * intersection + self.smooth) / \
               (inputs.sum() + targets.sum() + self.smooth)

        return 1 - dice


class DiceBCELoss(nn.Module):
    """
    Creates a criterion that outputs a combination of Dice score
    coefficient (DSC), a measure of overlap between two sets, and
    binary cross-entropy (BCE), the ubiquitous image segmentation
    measuring dissimilarity between two distributions. This metric's
    use case includes data with lightly imbalanced class epresentation
    (moderate to low foreground-to-background ratio), as it leverages
    the BCE smoothing to enhance the already-flexible performance of
    DSC.
    """

    # Constructor defines hyperparameters
    def __init__(self, smooth=1, alpha=.5):
        """
        :param float smooth: used to avoid division by 0 in Dice loss
                             score
        :param float alpha: weighted distribution of BinaryCrossEntropy
                            loss compared to DiceLoss
        """

        super(DiceBCELoss, self).__init__()
        self.smooth = smooth
        self.alpha = alpha

    def forward(self, inputs, targets):
        """
        :param inputs: tensor of size (N,∗), where N is the batch size
                       and * indicates any number of additional
                       dimensions
        :type inputs: List[float]
        :param targets: tensor of size (N,*), the same size as the input
        :type targets: List[float]
        """
        # Comment out if your model contains a sigmoid or equivalent activation
        # inputs = F.sigmoid(inputs)

        # Flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        intersection = (inputs * targets).sum()
        dice_loss = 1 - (2. * intersection + self.smooth) / (inputs.sum()
                                                             + targets.sum()
                                                             + self.smooth)
        bce = F.binary_cross_entropy(inputs, targets, reduction='mean')
        dice_bce = (self.alpha * bce) + ((1-self.alpha) * dice_loss)

        return dice_bce


class FocalLoss(nn.Module):
    """
    Creates a criterion that outputs the Focal loss metric, a variation
    of binary cross-entropy. Developed by Facebook AI Research in 2017
    as a means of applying segmentation on examples with low
    foreground-to-background ratios, the FocalLoss criterion
    down-weights the easy-to-detect class decisions and instead focuses
    training on hard negatives.
    """

    # Constructor defines hyperparameters
    def __init__(self, alpha=0.8, gamma=.5):
        """
        :param float alpha: balancing factor typically in range [0,1]
        :param float gamma: focusing parameter of modulating factor
                            (1-exp(-BCE)); FocalLoss is equal to cross
                            entropy when gamma is equal to 1
        """
        super(FocalLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma

    def forward(self, inputs, targets):
        """
        :param inputs: tensor of size (N,∗), where N is the batch size
                       and * indicates any number of additional
                       dimensions
        :type inputs: List[float]
        :param targets: tensor of size (N,*), the same size as the input
        :type targets: List[float]
        """
        # Comment out if your model contains a sigmoid or equivalent activation
        # inputs = F.sigmoid(inputs)

        # Flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        # first compute binary cross-entropy
        bce = F.binary_cross_entropy(inputs, targets, reduction='mean')
        bce_exp = torch.exp(-bce)
        focal_loss = self.alpha * (1 - bce_exp) ** self.gamma * bce

        return focal_loss


class TverskyLoss(nn.Module):
    """
    Creates a criterion that outputs the Tversky index loss, a
    generalization of the Dice loss coefficient, a measure of overlap
    between two sets that is widely popular in computer vision. The
    Tversky loss index aims to achieve a better precision-to-recall
    trade-off by adjusting how harshly false positives (FP) and false
    negatives (FN) are penalized.

    Hyperparameters alpha and beta control how harshly FPs and FNs are
    penalized, respectively, allowing the user to place higher emphasis
    on precision/recal with a larger alpha/beta value. For reference,
    setting alpha=beta=0.5 results in the DiceLoss criterion.
    """

    # Constructor defines hyperparameters
    def __init__(self, smooth=1, alpha=0.75, beta=0.25):
        """
        :param float smooth: used to avoid division by 0"
        :param float alpha: parameter that weights penalty of detecting
                            a false positive
        :param float beta: parameter that weights penalty of detecting a
                           false negative
        """
        super(TverskyLoss, self).__init__()
        self.smooth = smooth
        self.alpha = alpha
        self.beta = beta

    def forward(self, inputs, targets):
        """
        :param inputs: tensor of size (N,∗), where N is the batch size
                       and * indicates any number of additional
                       dimensions
        :type inputs: List[float]
        :param targets: tensor of size (N,*), the same size as the input
        :type targets: List[float]
        """
        # Comment out if your model contains a sigmoid or equivalent activation
        # inputs = F.sigmoid(inputs)

        # Flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        # True positives, false positives, and false negatives
        TP = (inputs * targets).sum()
        FP = ((1 - targets) * inputs).sum()
        FN = (targets * (1 - inputs)).sum()

        tversky_loss = (TP + self.smooth) / (TP + self.alpha
                                             * FP + self.beta
                                             * FN + self.smooth)

        return 1 - tversky_loss


class FocalTverskyLoss(nn.Module):
    """
    Creates a criterion that combines the customizable
    precision-to-recall penalty decisions of the Tversky loss (TL) index
    with the Focal loss (FL) advantages of dealing with hard examples
    with low foreground-to-background ratios.

    The FocalTverskyLoss (FTL) is mainly comprised of TL loss, a
    generalization of the Dice loss coefficient, a measure of overlap
    between two sets that is widely popular in computer vision. The TL
    index aims to achieve a better precision-to-recall trade-off by
    adjusting how harshly false positives (FP) and false negatives (FN)
    are penalized. Hyperparameters alpha and  beta control how harshly
    FPs and FNs are penalized, respectively, allowing the user to place
    higher emphasis on precision/recal with a larger alpha/beta value.
    For reference, setting alpha=beta=0.5 results in the DiceLoss
    criterion.

    Once the TL index is computed, hyperparameter gamma is introduced
    which down-weights the easy-to-detect class decisions and instead
    focuses training on hard negatives.
    """

    # Constructor defines hyperparameters
    def __init__(self, smooth=1, alpha=0.5, beta=0.5, gamma=.8):
        """
        :param float smooth: used to avoid division by 0
        :param float alpha: parameter that weights penalty of detecting
                            a false positive
        :param float beta: parameter that weights penalty of detecting a
                           false negative
        :param float gamma: focusing parameter of modulating factor
                            (1 - TL))
        """

        super(FocalTverskyLoss, self).__init__()
        self.smooth = smooth
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma

    def forward(self, inputs, targets):
        """
        :param inputs: tensor of size (N,∗), where N is the batch size
                       and * indicates any number of additional
                       dimensions
        :type inputs: List[float]
        :param targets: tensor of size (N,*), the same size as the input
        :type targets: List[float]
        """
        # Comment out if your model contains a sigmoid or equivalent activation
        # inputs = F.sigmoid(inputs)

        # Flatten label and prediction tensors
        inputs = inputs.view(-1)
        targets = targets.view(-1)

        # True positives, false positives, and false negatives
        TP = (inputs * targets).sum()
        FP = ((1 - targets) * inputs).sum()
        FN = (targets * (1 - inputs)).sum()

        tversky_loss = (TP + self.smooth) / (TP + self.alpha * FP +
                                             self.beta * FN + self.smooth)
        focal_tversky_loss = (1 - tversky_loss) ** self.gamma

        return focal_tversky_loss


def tst():
    """
    Defines and test several Mixed Scale Dense Networks consisting of 2D
    convolutions, provides a printout of the network, and checks to make
    sure tensors pass through the network
    """

    lossD = DiceLoss()
    lossDBCE = DiceBCELoss()
    lossF = FocalLoss()
    lossT = TverskyLoss()
    lossFT = FocalTverskyLoss()

    m = nn.Sigmoid()
    input_ = torch.randn(3, 5, requires_grad=True)
    target = torch.randn(3, 5)

    outputD = lossD(m(input_), target)
    outputDBCE = lossDBCE(m(input_), target)
    outputF = lossF(m(input_), target)
    outputT = lossT(m(input_), target)
    outputFT = lossFT(m(input_), target)

    outputD.backward()
    outputDBCE.backward()
    outputF.backward()
    outputT.backward()
    outputFT.backward()


if __name__ == "__main__":
    tst()
