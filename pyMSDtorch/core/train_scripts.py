import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
from pyMSDtorch.core import helpers
from pyMSDtorch.core import corcoef

import torchmetrics

from torchmetrics import MetricCollection, Accuracy, Precision, Recall, F1

def segmentation_metrics( preds, target):
    num_classes = preds.shape[1]
    tmp = torch.argmax(preds, dim=1)
    F1_eval = F1(num_classes=num_classes,
                 average='macro',
                 mdmc_average='global')
    tmp = F1_eval(tmp.cpu(), target.cpu())

    #tmp = torch.tensor([0.0])
    return(tmp)

def train_segmentation(net, trainloader, validationloader, NUM_EPOCHS,
                       criterion, optimizer, device, scheduler=None, show=0):
    """
    Loop through epochs passing dirty images to net. Denoised images saved to
    MSD_Saved_Images folder

    Input:
        :param net: The neural network
        :param trainloader: Set of data to train. created via torch.utils.data
                            import DataLoader
        :param NUM_EPOCHS: number of training epochs
    Output:
        :returns train_loss: running_loss defined by 'criterion' above
        (usually nn.MSELoss() ). Normalized by number of images in batch
        (running_loss / len(trainloader) )
    """
    train_loss = []
    validation_loss = []
    F1_train_trace = []
    F1_validation_trace= []
    for epoch in range(NUM_EPOCHS):
        running_train_loss = 0.0
        running_validation_loss = 0.0
        running_F1_train_val = 0.0
        running_F1_validation_val = 0.0
        tot_train = 0.0
        tot_val = 0.0
        count=0
        for data in trainloader:
            count+=1
            noisy, target = data  # load noisy and target images
            N_train = noisy.shape[0]
            tot_train+=N_train

            #noisy = noisy.type(torch.FloatTensor)
            #target = target.type(torch.LongTensor)
            noisy = noisy.to(device)
            target = target.to(device)



            if criterion.__class__.__name__ == 'CrossEntropyLoss':
                target = target.type(torch.LongTensor)
                target = target.to(device).squeeze(1)

            # forward pass, compute loss and accuracy
            output = net(noisy)

            tmp = segmentation_metrics(output, target)
            running_F1_train_val += tmp.item() #*N_train
            loss = criterion(output, target)

            # backpropagation
            optimizer.zero_grad()
            loss.backward()

            # update the parameters
            optimizer.step()
            if scheduler is not None:
                scheduler.step()

            running_train_loss += loss.item()

        # compute validation step
        with torch.no_grad():
            for x, y in validationloader:
                x = x.to(device)
                y = y.to(device)
                N_val = y.shape[0]
                tot_val += N_val
                if criterion.__class__.__name__ == 'CrossEntropyLoss':
                    y = y.type(torch.LongTensor)
                    y = y.to(device).squeeze(1)

                # forward pass, compute validation loss and accuracy
                # net.eval()
                yhat = net(x)
                val_loss = criterion(yhat, y)

                tmp = segmentation_metrics(yhat, y)
                running_F1_validation_val += tmp.item() #*N_val

                # update running validation loss and accuracy
                running_validation_loss += val_loss.item()

        loss = running_train_loss / len(trainloader)
        val_loss = running_validation_loss / len(validationloader)
        F1 = running_F1_train_val / len(trainloader)
        F1_val = running_F1_validation_val / len(validationloader)

        train_loss.append(loss)
        validation_loss.append(val_loss)
        F1_train_trace.append(F1)
        F1_validation_trace.append(F1_val)
        if show != 0:
            if np.mod(epoch + 1, show) == 0:
                print(
                    f'Epoch {epoch + 1} of {NUM_EPOCHS} | Training Loss: {loss:.4f} | Validation Loss: {val_loss:.4f}')
                print(f'                   Training F1: {F1:.4f}   Validation F1  : {F1_val:.4f} ')

    results = {"Training loss": train_loss,
               "Validation loss": validation_loss,
               "F1 training": F1_train_trace,
               "F1 validation": F1_validation_trace}

    return net, results


def regression_metrics( preds, target):
    tmp = corcoef.cc(preds.cpu().flatten(), target.cpu().flatten() )
    return(tmp)



def train_regression(net, trainloader, validationloader, NUM_EPOCHS,
                       criterion, optimizer, device, scheduler=None, show=0):
    """
    Loop through epochs passing dirty images to net. Denoised images saved to
    MSD_Saved_Images folder

    Input:
        :param net: The neural network
        :param trainloader: Set of data to train. created via torch.utils.data
                            import DataLoader
        :param NUM_EPOCHS: number of training epochs
    Output:
        :returns train_loss: running_loss defined by 'criterion' above
                             (usually nn.MSELoss() ). Normalized by number
                             of images in batch (running_loss/len(trainloader))
    """
    train_loss = []
    validation_loss = []
    CC_train_trace = []
    CC_validation_trace= []

    for epoch in range(NUM_EPOCHS):
        running_train_loss = 0.0
        running_validation_loss = 0.0
        running_CC_train_val = 0.0
        running_CC_validation_val = 0.0
        tot_train = 0.0
        tot_val = 0.0
        count=0
        for data in trainloader:
            count+=1
            noisy, target = data  # load noisy and target images
            N_train = noisy.shape[0]
            tot_train+=N_train

            noisy = noisy.type(torch.FloatTensor)
            target = target.type(torch.FloatTensor)
            noisy = noisy.to(device)
            target = target.to(device)

            # forward pass, compute loss and accuracy
            output = net(noisy)

            tmp =  regression_metrics(output, target)

            running_CC_train_val += tmp.item() #*N_train
            loss = criterion(output, target)

            # backpropagation
            optimizer.zero_grad()
            loss.backward()

            # update the parameters
            optimizer.step()
            if scheduler is not None:
                scheduler.step()

            running_train_loss += loss.item()

        # compute validation step
        with torch.no_grad():
            for x, y in validationloader:
                x = x.to(device)
                y = y.to(device)
                N_val = y.shape[0]
                tot_val += N_val
                if criterion.__class__.__name__ == 'CrossEntropyLoss':
                    y = y.type(torch.LongTensor)
                    y = y.to(device).squeeze(1)

                # forward pass, compute validation loss and accuracy
                # net.eval()
                yhat = net(x)
                val_loss = criterion(yhat, y)

                tmp = regression_metrics(yhat, y)
                running_CC_validation_val += tmp.item() #*N_val

                # update running validation loss and accuracy
                running_validation_loss += val_loss.item()

        loss = running_train_loss / len(trainloader)
        val_loss = running_validation_loss / len(validationloader)
        CC = running_CC_train_val / len(trainloader)
        CC_val = running_CC_validation_val / len(validationloader)

        train_loss.append(loss)
        validation_loss.append(val_loss)
        CC_train_trace.append(CC)
        CC_validation_trace.append(CC_val)
        if show != 0:
            if np.mod(epoch + 1, show) == 0:
                print(
                    f'Epoch {epoch + 1} of {NUM_EPOCHS} | Training Loss: {loss:.4f} | Validation Loss: {val_loss:.4f}')
                print(f'                   Training CC: {CC:.4f}   Validation CC  : {CC_val:.4f} ')

    results = {"Training loss": train_loss,
               "Validation loss": validation_loss,
               "CC training": CC_train_trace,
               "CC validation": CC_validation_trace}

    return net, results

def tst():
    print('Add a test')

if __name__ == "__main__":
    tst()