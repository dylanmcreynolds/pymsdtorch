import torch

def cov(m):
    fact = 1.0 / (m.shape[-1] - 1)  # 1 / N
    m -= torch.mean(m, dim=(1, 2), keepdim=True)
    mt = torch.transpose(m, 1, 2)
    return fact * m.matmul(mt).squeeze()


def compute_rank_correlation(x, y):
    x, y = rankmin(x), rankmin(y)
    return corrcoef(x, y)


def corrcoef(x, y):
    batch_size = x.shape[0]
    x = torch.stack((x, y), 1)
    # calculate covariance matrix of rows
    c = cov(x)
    # normalize covariance matrix
    d = torch.diagonal(c, dim1=1, dim2=2)
    stddev = torch.pow(d, 0.5)
    stddev = stddev.repeat(1, 2).view(batch_size, 2, 2)
    c = c.div(stddev)
    c = c.div(torch.transpose(stddev, 1, 2))
    return c[:, 1, 0]

def cc(a,b):
    ma = torch.mean(a)
    mb = torch.mean(b)
    va = torch.mean(a*a)
    vb = torch.mean(b*b)
    mab = torch.mean(a*b)
    sa = torch.sqrt(va - ma*ma)
    sb = torch.sqrt(vb - mb*mb)
    result_top =  mab - ma*mb
    result_bottom = sa*sb
    return result_top/result_bottom

